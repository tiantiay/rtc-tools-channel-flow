within Deltares.ChannelFlow.Hydraulic.Branches.Internal;

type BottomFrictionCoefficient = Real(final unit = "1/s");
